package company;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/6 15:01
 */
public abstract class AbstratCompany {
    public abstract void add(AbstratCompany abstratCompany);
    public abstract void remove(AbstratCompany abstratCompany);
    public abstract void sendMsg();
}
