package company;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/6 15:18
 */
public class CompanyMain {
    public static void main(String[] args) {
        AbstratCompany branch1,branch2,total;
        AbstratCompany dept1,dept2,dept3,dept4,dept5,dept6,dept7,dept8,dept9;
        total=new branchCompany("总公司");
        branch1=new branchCompany("上海分公司");
        branch2=new branchCompany("深圳分公司");
        dept1=new DevelopmentDept("总公司研发部");
        dept2=new FinanceDept("总公司财务部");
        dept3=new DevelopmentDept("上海分公司研发部");
        dept4=new FinanceDept("上海分公司财务部");
        dept5=new HumanResouceDept("上海分公司人力资源管理部");
        dept6=new DevelopmentDept("上海分公司研发部");
        dept7=new FinanceDept("上海分公司财务部");
        dept8=new HumanResouceDept("上海分公司人力资源管理部");
        total.add(branch1);
        total.add(branch2);
        branch1.add(dept3);
        branch1.add(dept4);
        branch1.add(dept5);
        branch2.add(dept6);
        branch2.add(dept7);
        branch2.add(dept8);
        total.sendMsg();

    }
}
