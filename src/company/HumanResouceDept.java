package company;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/6 15:28
 */
public class HumanResouceDept extends AbstratCompany {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HumanResouceDept(String name) {
        this.name = name;
    }

    @Override
    public void add(AbstratCompany abstratCompany) {
        System.out.println("禁止使用");
    }

    @Override
    public void remove(AbstratCompany abstratCompany) {
        System.out.println("禁止使用");
    }

    @Override
    public void sendMsg() {
        System.out.println(name+"收到消息");
    }
}

