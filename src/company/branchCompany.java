package company;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/6 15:30
 */
public class branchCompany extends AbstratCompany{
    private String name;
    List<AbstratCompany> abstratCompanies=new ArrayList<>();
    @Override
    public void add(AbstratCompany abstratCompany) {
        abstratCompanies.add(abstratCompany);
    }

    @Override
    public void remove(AbstratCompany abstratCompany) {
        abstratCompanies.remove(abstratCompany);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public branchCompany(String name) {
        this.name = name;
    }

    @Override
    public void sendMsg() {
        for (AbstratCompany abstratCompany : abstratCompanies) {
            abstratCompany.sendMsg();
        }
    }
}
