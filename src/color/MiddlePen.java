package color;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/5 10:41
 */
public class MiddlePen extends Pen{
    private String penType="中号毛笔";
    @Override
    public void draw(String name) {
        color.bePaint(penType,name);
    }
}
