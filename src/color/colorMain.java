package color;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/5 10:42
 */
public class colorMain {
    public static void main(String[] args) {
        Pen pen;
        Color color;
        pen= (Pen) ReadXml.getBean("pen");
        color= (Color) ReadXml.getBean("color");
        pen.setColor(color);
        pen.draw("鲜花");

    }
}
