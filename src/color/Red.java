package color;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/5 10:32
 */
public class Red implements Color{
    @Override
    public void bePaint(String penType, String name) {
        System.out.println("用"+penType+"画出红色的"+name+".");
    }
}
