package color;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/5 10:30
 */
public interface Color {
    void bePaint(String penType,String name);

}
