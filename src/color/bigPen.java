package color;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/5 10:41
 */
public class bigPen extends Pen{
    private String penType="大号毛笔";
    @Override
    public void draw(String name) {
        color.bePaint(penType,name);
    }
}
