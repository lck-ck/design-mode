package color;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/5 10:33
 */
public class blue implements Color{
    @Override
    public void bePaint(String penType, String name) {
        System.out.println("用"+penType+"画出蓝色的"+name+".");
    }
}
