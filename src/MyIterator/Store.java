package MyIterator;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/2 11:13
 */
public interface Store {
    WDIterator createIterator();
}
