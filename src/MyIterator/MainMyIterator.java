package MyIterator;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/2 11:27
 */
public class MainMyIterator {
    public static void main(String[] args) {
        WDStore store = new WDStore();
        WDIterator wdIterator = store.createIterator();
        Object o = wdIterator.currentItem();
        while (!wdIterator.hasNext()) {
            System.out.println(wdIterator.currentItem());
            wdIterator.next();
        }

        while (!wdIterator.first()) {
            wdIterator.previous();
            System.out.println(wdIterator.currentItem());

        }
    }
}
