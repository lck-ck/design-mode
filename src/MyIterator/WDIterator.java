package MyIterator;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/2 11:19
 */
public interface WDIterator {
    boolean first();
    void next();
    void previous();
    boolean hasNext();
    Object currentItem();
}
