package MyIterator;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/2 11:14
 */
public class WDStore implements Store{
    private Object[] objects = {"草鱼", "牛肉", "白菜", "豆角", "玉米"};

    @Override
    public myIterator createIterator() {
        return new myIterator();
    }

    private class myIterator implements WDIterator
    {
        private WDStore store=new WDStore();
        private int current=0;

        @Override
        public boolean first() {
            return current<1;
        }

        @Override
        public void next() {
            current++;
        }

        @Override
        public void previous() {
            current--;
        }

        @Override
        public boolean hasNext() {
            return current>=store.objects.length;
        }

        @Override
        public Object currentItem() {
            return objects[current];
        }
    }
}
