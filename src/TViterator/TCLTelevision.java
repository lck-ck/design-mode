package TViterator;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/2 10:32
 */
public class TCLTelevision implements Television {
    private Object[] objects = {"中央一台", "中央二台", "中央三台", "中央四台", "中央五台"};

    @Override
    public TVIterator creatIterator() {
        return new TCLIterator();
    }

    private class TCLIterator implements TVIterator {
        private TCLTelevision tv = new TCLTelevision();
        private int current = 0;


        @Override
        public void setChannel(int i) {
            current = i;
        }

        @Override
        public Object currentChannel() {
            return objects[current];
        }

        @Override
        public void next() {
            current++;
        }

        @Override
        public void previous() {
            current--;
        }

        @Override
        public boolean isFirst() {
            return current < 0;
        }

        @Override
        public boolean isLast() {
            return current >= tv.objects.length;
        }
    }
}