package TViterator;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/2 10:31
 */
public interface TVIterator {
    void setChannel(int i);
    Object currentChannel();
    void next();
    void previous();
    boolean isFirst();
    boolean isLast();

}
