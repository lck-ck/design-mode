package TViterator;

import java.util.*;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/2 10:44
 */
public class Main {
    public static void main(String[] args) {
        Television tv=new TCLTelevision();
        TVIterator i=tv.creatIterator();
//        while (!i.isLast()){
//            System.out.println(i.currentChannel().toString());
//            i.next();
//        }
        i.setChannel(4);
        while (!i.isFirst()){
//            System.out.println(i.currentChannel().toString());
            i.previous();
        }
        List person=new ArrayList();
        person.add("1");
        person.add("2");
        person.add("3");
        person.add("4");

        ListIterator iterator=person.listIterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next().toString());
        }
        while (iterator.hasPrevious())
        {
            System.out.println(iterator.previous().toString());
        }
    }
}
