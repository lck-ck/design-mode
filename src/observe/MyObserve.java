package observe;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:27
 */
public interface MyObserve {
    void response();
}
