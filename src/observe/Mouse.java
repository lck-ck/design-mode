package observe;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:33
 */
public class Mouse implements MyObserve {
    @Override
    public void response() {
        System.out.println("老鼠拼命的逃跑");
    }
}
