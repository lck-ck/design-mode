package observe;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:34
 */
public class observeMain {
    public static void main(String[] args) {
        MySubject subject=new Cat();
        MyObserve observe1,observe2,observe3;
        observe1=new Mouse();
        observe2=new Dog();
        observe3=new Pig();
        subject.attach(observe1);
        subject.attach(observe2);
        subject.attach(observe3);
        subject.cry();
    }
}
