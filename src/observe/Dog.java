package observe;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:33
 */
public class Dog implements MyObserve {
    @Override
    public void response() {
        System.out.println("狗也跟着叫");

    }
}
