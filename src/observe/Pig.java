package observe;

import javax.swing.*;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:36
 */
public class Pig implements MyObserve{
    @Override
    public void response() {
        System.out.println("猪没反应，还是在睡觉");
    }
}
