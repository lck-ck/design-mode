package observe;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:30
 */
public class Cat extends MySubject{
    @Override
    public void cry() {
        System.out.println("猫喵喵叫");
        for (Object o : observes) {
            ((MyObserve)o).response();
        }
    }
}
