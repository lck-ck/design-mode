package observe;

import java.util.ArrayList;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:28
 */
public abstract class MySubject {
    protected ArrayList observes=new ArrayList();
    public void attach(MyObserve observe){
        observes.add(observe);
    }
    public void detach(MyObserve observe)
    {
        observes.remove(observe);
    }
    public abstract void cry();
}
