package phone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/12 11:14
 */
public class PhoneMain {
    public static void main(String[] args) {
        SimplePhone simplePhone1,simplePhone2,simplePhone3;
        simplePhone1=new VoicePhone();
        simplePhone2=new ComplexPhone(simplePhone1);
        simplePhone2.callMessage();
    }
}
