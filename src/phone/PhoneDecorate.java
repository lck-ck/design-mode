package phone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/12 11:56
 */
public class PhoneDecorate extends SimplePhone {
    SimplePhone simplePhone;

    public PhoneDecorate(SimplePhone simplePhone) {
        this.simplePhone = simplePhone;
       // simplePhone.callMessage();
    }

    @Override
    public void callMessage() {
        System.out.println("震动");
        simplePhone.callMessage();
    }
}
