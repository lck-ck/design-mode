package phone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/12 10:55
 */
public abstract class SimplePhone {
    public abstract void callMessage();
}
