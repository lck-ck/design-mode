package planeSort;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 11:40
 */
public interface CharacteristicsOfFlight {
    void play();
}
