package planeSort;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 11:43
 */
public class planeMain {
    public static void main(String[] args) {
        PlaneHandler handler=new PlaneHandler();
        CharacteristicsOfFlight flight= (CharacteristicsOfFlight) ReadXML.getBean();
        flight.play();
    }
}
