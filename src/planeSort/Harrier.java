package planeSort;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 11:41
 */
public class Harrier implements CharacteristicsOfFlight{
    @Override
    public void play() {
        System.out.println("垂直起飞+超音速飞行");
    }
}
