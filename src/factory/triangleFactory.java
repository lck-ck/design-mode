package factory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 15:47
 */
public class triangleFactory implements Factory{
    @Override
    public shape produce() {
        return  new triangle();
    }
}
