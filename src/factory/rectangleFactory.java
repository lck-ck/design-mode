package factory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 15:49
 */
public class rectangleFactory implements Factory{
    @Override
    public shape produce() {
        return new rectangle();
    }
}
