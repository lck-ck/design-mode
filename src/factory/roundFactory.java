package factory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 15:46
 */
public class roundFactory implements Factory {
    @Override
    public shape produce() {
        return new round();
    }
}
