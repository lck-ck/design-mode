package factory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 15:35
 */
public class round implements shape {

    @Override
    public void draw() {
        System.out.println("我是圆形draw方法");
    }

    @Override
    public void erase() {
        System.out.println("我是圆形erase方法");
    }
}
