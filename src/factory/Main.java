package factory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 15:52
 */
public class Main {
    public static void main(String[] args) {
        shape shape;
        Factory factory;
        factory = (Factory) XMLRead.getBrand();
        shape= factory.produce();
        shape.draw();
    }
}
