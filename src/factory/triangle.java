package factory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 15:37
 */
public class triangle implements shape{
    @Override
    public void draw() {
        System.out.println("我是三角形draw方法");
    }

    @Override
    public void erase() {
        System.out.println("我是三角形erase方法");
    }
}
