package factory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 15:39
 */
public class rectangle implements shape{


    @Override
    public void draw() {
        System.out.println("我是方形draw方法");
    }

    @Override
    public void erase() {
        System.out.println("我是方形erase方法");
    }
}
