package factory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 15:45
 */
public interface Factory {
    shape produce();
}
