package test111;

public class NormalState extends AccountState{
    public NormalState(Account acc) {
        this.acc=acc;
    }

    public NormalState(AccountState state) {
        this.acc=state.acc;
    }
    @Override
    public void deposit(double amount) {
        acc.setBalance(acc.getBalance()+amount);
        stateCheck();
    }

    @Override
    public void withdraw(double amount) {
        acc.setBalance(acc.getBalance()-amount);
        stateCheck();
    }

    @Override
    public void computeInterest() {
        System.out.println("不需要计算利息");
    }

    @Override
    public void stateCheck() {
if(acc.getBalance()<0){
    acc.setState(new OverdraftState(this));
}
else  if(acc.getBalance()==-2000){
    acc.setState(new RestricedState(this));
}
    }
}
