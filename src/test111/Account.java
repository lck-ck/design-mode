package test111;

public class Account {
    protected  AccountState state;
    protected String owner;
    protected  double balance=0;

    public Account(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
        this.state=new NormalState(this);
        System.out.println(this.owner+"开户初始金额为"+this.balance);
    }

    public AccountState getState() {
        return state;
    }

    public void setState(AccountState state) {
        this.state = state;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    public void deposit(double amount){
        System.out.println(owner+"存款");
        state.deposit(amount);
        System.out.println("现在账户状态为"+state.getClass().getName());
    }
    public void withdraw(double amount){
        System.out.println("取钱");
        state.withdraw(amount);
        System.out.println("现在账户状态为"+state.getClass().getName());

    }
    public void computeInterest(){
state.computeInterest();
    }
}
