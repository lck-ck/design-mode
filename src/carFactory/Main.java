package carFactory;


import factory.shape;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 15:52
 */
public class Main {
    public static void main(String[] args) {
        Car car;
        Factory2 factory2;
        factory2 = (Factory2) XMLRead.getBrand();
        car= factory2.produce();
        car.run();
    }
}
