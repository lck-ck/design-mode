package carFactory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 17:07
 */
public class BYDfactory implements Factory2{
    @Override
    public Car produce() {
        return new BYD();
    }
}
