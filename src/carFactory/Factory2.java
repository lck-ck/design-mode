package carFactory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 16:59
 */
public interface Factory2 {
    Car produce();
}
