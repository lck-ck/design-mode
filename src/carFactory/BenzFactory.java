package carFactory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 17:03
 */
public class BenzFactory implements Factory2{
    @Override
    public Car produce() {
        return new Benz();
    }
}
