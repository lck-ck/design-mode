package carFactory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 17:06
 */
public class BYD implements Car {
    @Override
    public void run() {
        System.out.println("我是比亚迪汽车");
    }
}
