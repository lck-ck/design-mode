package carFactory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 17:03
 */
public class Benz implements Car {
    @Override
    public void run() {
        System.out.println("我是奔驰");
    }
}
