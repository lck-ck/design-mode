package carFactory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 17:00
 */
public interface Car {
    void run();
}
