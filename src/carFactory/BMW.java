package carFactory;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/22 17:01
 */
public class BMW implements Car {
    @Override
    public void run() {
        System.out.println("我是宝马");
    }
}
