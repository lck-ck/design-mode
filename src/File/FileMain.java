package File;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/6 14:39
 */
public class FileMain {
    public static void main(String[] args) {
        AbstractFile file1,file2,file3,file4;
        AbstractFile folder1,folder2,folder3;
        folder1=new Folder("资料库");
        folder2=new Folder("视频");
        folder3=new Folder("音频");
        file1=new TextFile("九阴真经.txt");
        file2=new TextFile("葵花宝典.txt");
        file3=new ImageFile("杨过.jpg");
        file4=new VideoFile("录像.mp4");
        folder1.add(folder2);
        folder1.add(folder3);
        folder2.add(file1);
        folder2.add(file2);
        folder3.add(file3);
        folder3.add(file4);
        folder1.killvirus();

    }
}
