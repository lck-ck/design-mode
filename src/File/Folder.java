package File;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/6 14:35
 */
public class Folder extends AbstractFile{
    private String name;
    private List<AbstractFile> abstractFiles=new ArrayList<>();

    public Folder(String name) {
        this.name = name;
    }

    @Override
    public void add(AbstractFile abstractFile) {
        abstractFiles.add(abstractFile);
    }

    @Override
    public void remove(AbstractFile abstractFile) {
        abstractFiles.remove(abstractFile);
    }

    @Override
    public AbstractFile getFile(int i) {
        return abstractFiles.get(i);
    }

    @Override
    public void killvirus() {
        for (AbstractFile abstractFile:abstractFiles){
            abstractFile.killvirus();
        }
    }
}
