package File;

import java.security.PublicKey;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/6 14:29
 */
public abstract class AbstractFile {
    public abstract void add(AbstractFile abstractFile);
    public abstract void remove(AbstractFile abstractFile);
    public abstract AbstractFile getFile(int i);
    public abstract void killvirus();
}
