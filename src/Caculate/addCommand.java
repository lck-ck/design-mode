package Caculate;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:52
 */
public class addCommand implements AbstractCommand{
    private Adder adder;
    private List list=new ArrayList();
    public addCommand() {
        adder=new Adder();
    }

    @Override
    public int execute(int value) {
        list.add(value);
        return adder.add(value);
    }

    @Override
    public int undo() {
        if (list.size()>0)
        {
            int i= (int) list.get(list.size()-1);
            list.remove(list.size()-1);
            return adder.add(-i);
        }
        else {
            return 0;
        }


    }
}
