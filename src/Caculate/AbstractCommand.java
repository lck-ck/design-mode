package Caculate;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:50
 */
public interface AbstractCommand {
    int execute(int value);
    int undo();
}
