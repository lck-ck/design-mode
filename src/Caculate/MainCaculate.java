package Caculate;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:48
 */
public class MainCaculate {
    public static void main(String[] args) {
        AbstractCommand command1=new addCommand();

        CalculatorForm calculatorForm=new CalculatorForm(command1);
        calculatorForm.execute(5);
        calculatorForm.execute(5);
        calculatorForm.execute(25);
        calculatorForm.execute(5);
        calculatorForm.execute(5);
        calculatorForm.undo();
        calculatorForm.undo();
        calculatorForm.undo();

    }
}
