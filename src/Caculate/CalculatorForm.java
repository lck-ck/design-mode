package Caculate;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:47
 */
public class CalculatorForm {
private AbstractCommand command;

    public CalculatorForm(AbstractCommand command) {
        this.command = command;
    }
    public void execute(int value){
        int i= command.execute(value);
        System.out.println("计算结果是："+i);
    }
    public void undo()
    {
        int i= command.undo();
        System.out.println("计算结果是："+i);
    }
}
