package buider;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 10:53
 */
public class DevilBuilder extends Builder{
    @Override
    public void typeBuilder() {
        actor.setType("恶魔");
    }

    @Override
    public boolean isBareHeaded() {
        return true;
    }

    @Override
    public void sexBuilder() {
        actor.setSex("男");

    }

    @Override
    public void costumeBuilder() {
        actor.setCostume("黑袍");

    }

    @Override
    public void hairStyleBuilder() {
        actor.setHairStyle("光头");
    }
}
