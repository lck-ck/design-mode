package buider;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 10:51
 */
public class AngleBuilder extends Builder{
    @Override
    public void typeBuilder() {
        actor.setType("天使");
    }

    @Override
    public void sexBuilder() {
        actor.setSex("女");

    }

    @Override
    public void costumeBuilder() {
        actor.setCostume("长裙");

    }

    @Override
    public void hairStyleBuilder() {
        actor.setHairStyle("披肩长发");
    }
}
