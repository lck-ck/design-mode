package buider;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 10:44
 */
public class main {
    public static void main(String[] args) {
        Actor actor;
        Builder builder=new AngleBuilder();
        Director director=new Director();
        actor= director.construct(builder);
        System.out.println(actor);

    }
}
