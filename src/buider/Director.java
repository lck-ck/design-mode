package buider;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 10:58
 */
public class Director {
    public Director() {
    }
    public Actor construct(Builder builder){
        Actor actor;
        builder.sexBuilder();
        builder.typeBuilder();
        if (!builder.isBareHeaded()){
            builder.hairStyleBuilder();
        }
        return builder.CreateActor();

    }
}
