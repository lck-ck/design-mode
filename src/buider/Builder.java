package buider;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 10:47
 */
public abstract class Builder {
    Actor actor=new Actor();
    public abstract void typeBuilder();
    public abstract void sexBuilder();
    public abstract void costumeBuilder();
    public abstract void hairStyleBuilder();
    public boolean isBareHeaded()
    {
        return false;
    }
    public Actor CreateActor(){
        return actor;
    }
}
