package Forum;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 14:46
 */
public class PrimaryState extends AbstractState{
    public PrimaryState(ForumAccount acc) {
        this.acc=acc;
        this.point=0;
        this.stateName="新手";
    }

    public PrimaryState(AbstractState state) {
        this.acc=state.acc;
        this.point=state.getPoint();
        this.stateName="新手";
    }

    @Override
    public void writeNote(int score) {
        this.point+=score;
        checkState();
    }

    @Override
    public void replyNote(int score) {
        this.point+=score;
        checkState();
    }

    @Override
    public void download(int score) {
        System.out.println("新手无法下载文件");
    }
}
