package Forum;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 15:01
 */
public class forumMain {
    public static void main(String[] args) {
        ForumAccount acc=new ForumAccount("张三");
        System.out.println(acc.getState());
        acc.writeNote(120);
        System.out.println(acc.getState());
        acc.downFile(100);
        System.out.println(acc.getState());
    }
}
