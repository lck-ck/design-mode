package Forum;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 14:50
 */
public class MiddleState extends AbstractState{
    public MiddleState(AbstractState state) {
        this.acc=state.acc;
        this.point=state.getPoint();
        this.stateName="高手";
    }

    @Override
    public void writeNote(int score) {
        this.point+=score*2;
        checkState();
    }

    @Override
    public void replyNote(int score) {
        this.point+=score*2;
        checkState();
    }

    @Override
    public void download(int score) {
        if (this.point-score<0)
        {
            System.out.println("你的积分够下载文件");
        }else
        {
            this.point-=score;
            checkState();
        }
    }
}
