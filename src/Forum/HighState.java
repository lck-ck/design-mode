package Forum;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 14:52
 */
public class HighState extends AbstractState{
    public HighState(AbstractState state) {
        this.acc=state.acc;
        this.point=state.getPoint();
        this.stateName="专家";
    }

    @Override
    public void writeNote(int score) {
        this.point+=score*2;
        checkState();
    }

    @Override
    public void replyNote(int score) {
        this.point+=score*2;
        checkState();
    }

    @Override
    public void download(int score) {
        if (this.point-score/2<0)
        {
            System.out.println("你的积分够下载文件");
        }else
        {
            this.point-=score/2;
            checkState();
        }
    }
}
