package Forum;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 14:39
 */
public abstract class AbstractState {
    protected ForumAccount acc;

    protected String stateName;

    protected int point;

    public ForumAccount getAcc() {
        return acc;
    }

    public void setAcc(ForumAccount acc) {
        this.acc = acc;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
    public abstract void writeNote(int score);
    public abstract void replyNote(int score);
    public abstract void download(int score);

    @Override
    public String toString() {
        return "AbstractState{" +
                "acc=" + acc.getAccountName() +
                ", stateName='" + stateName + '\'' +
                ", point=" + point +
                '}';
    }

    public void checkState()
    {
        if (this.point>=1000)
        {
            this.acc.setState(new HighState(this));
        }else if (this.point>=100)
        {
            this.acc.setState(new MiddleState(this));
        }else
        {
            this.acc.setState(new PrimaryState(this));
        }
    }
}
