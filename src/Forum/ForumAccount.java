package Forum;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 14:39
 */
public class ForumAccount {
    private String accountName;
    private AbstractState state;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public AbstractState getState() {
        return state;
    }

    public void setState(AbstractState state) {
        this.state = state;
    }

    public ForumAccount(String accountName) {
        this.accountName = accountName;
        state=new PrimaryState(this);
    }
    public void downFile(int score)
    {
        this.state.download(score);
    }
    public  void writeNote(int score)
    {
        this.state.writeNote(score);
    }
    public void replyNote(int score)
    {
        this.state.replyNote(score);
    }
}
