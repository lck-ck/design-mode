package Game;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:49
 */
public class ConcreteAllyControlCenter extends AllyControlCenter{
    public ConcreteAllyControlCenter(String allyName) {
        super(allyName);
    }

    @Override
    public void notifyObserve(String name) {
        System.out.println(name+"正在受到攻击");
        for (Object o : players) {
            if (!((MyObserve)o).name.equalsIgnoreCase(name))
            {
                ((MyObserve)o).help();
            }
        }
    }
}
