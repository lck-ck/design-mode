package Game;

import java.util.ArrayList;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:44
 */
public abstract class AllyControlCenter {
    protected String allyName;
    protected ArrayList players=new ArrayList();

    public AllyControlCenter(String allyName) {
        this.allyName = allyName;
        System.out.println(this.allyName+"战队组件成功");
    }

    public String getAllyName() {
        return allyName;
    }

    public void setAllyName(String allyName) {
        this.allyName = allyName;
    }

    public ArrayList getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList players) {
        this.players = players;
    }
    public void join(MyObserve observe){
        players.add(observe);
        System.out.println(observe.getName()+"加入"+this.allyName);
    }
    public void quit(MyObserve observe)
    {
        players.remove(observe);
    }
    public abstract void notifyObserve(String name);
}
