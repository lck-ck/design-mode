package Game;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:42
 */
public abstract class MyObserve {
    protected String name;

    public MyObserve(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public abstract void help();
    public abstract void beAttacked(AllyControlCenter acc);
}
