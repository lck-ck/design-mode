package Game;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:51
 */
public class Player extends MyObserve{
    public Player(String name) {
        super(name);
    }

    @Override
    public void help() {
        System.out.println("坚持住"+this.name+"马上来帮你");
    }

    @Override
    public void beAttacked(AllyControlCenter acc) {
        acc.notifyObserve(this.name);
    }
}
