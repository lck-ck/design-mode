package Game;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 10:56
 */
public class GameMain {
    public static void main(String[] args) {
        AllyControlCenter team=new ConcreteAllyControlCenter("计科3，4班英雄联盟战队");
        MyObserve player1,player2,player3,player4,player5;
        player1=new Player("黄州");
        player2=new Player("陆建君");
        player3=new Player("何福建");
        player4=new Player("刘磊");
        player5=new Player("邱旭");
        team.join(player1);
        team.join(player2);
        team.join(player3);
        team.join(player4);
        team.join(player5);

        player2.beAttacked(team);
    }
}
