package decorate;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/12 10:34
 */
public abstract class Component {
    public abstract void display();
}
