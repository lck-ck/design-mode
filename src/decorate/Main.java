package decorate;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/12 10:39
 */
public class Main {

    public static void main(String[] args) {
        Component component1,component2,component3;
        component1=new TextBox();
        component2=new BlackBorderDecoretor(component1);
        //component2.display();
        component3=new ScrollBarDecorator(component2);
        component3.display();

    }
}
