package decorate;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/12 10:38
 */
public class Decorator extends Component{
    Component component;

    public Decorator(Component component) {
        this.component = component;
    }

    @Override
    public void display() {
        System.out.print("滚动");
        component.display();
    }
}
