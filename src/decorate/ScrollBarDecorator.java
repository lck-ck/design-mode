package decorate;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/12 10:41
 */
public class ScrollBarDecorator extends Decorator {

    @Override
    public void display() {
        this.setDecorator();
        super.display();
    }

    public void setDecorator() {
        System.out.println("滚动");
    }

    public ScrollBarDecorator(Component component) {
        super(component);
    }
}
