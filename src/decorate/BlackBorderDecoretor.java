package decorate;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/12 10:43
 */
public class BlackBorderDecoretor extends Decorator {
    @Override
    public void display() {
        this.setDecorator();
        super.display();
    }

    public void setDecorator() {
        System.out.println("黑色");
    }

    public BlackBorderDecoretor(Component component) {
        super(component);
    }
}
