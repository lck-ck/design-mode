package UI;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class GetImg implements Cloneable{
    public static BufferedImage getImg(String name)
    {
        BufferedImage image=null;
        try {
            image= ImageIO.read(GetImg.class.getResource("/img/"+name));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return image;
    }
}
