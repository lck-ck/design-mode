package UI;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        Frame frame = new Frame();
        Panel panel = new Panel(frame);
        panel.firebegin();
        panel.begin();
        frame.add(panel);
        frame.setVisible(true);
    }
}
