package UI;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Random;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/8 17:17
 */
public class EnermyPlayer implements Cloneable,Serializable {
    BufferedImage image;
    int x;
    int y;

    public EnermyPlayer() {
        Random random=new Random();
        int index=random.nextInt(15)+1;
        String path = (index<10)?("0"+index):(""+index);
        String name="ep"+path+".png";
        image=GetImg.getImg(name);
        y=0;
        x=random.nextInt(512- image.getWidth());
    }

    @Override
    protected EnermyPlayer clone() throws CloneNotSupportedException {
        return (EnermyPlayer) super.clone();
    }
    protected EnermyPlayer deepClone() throws CloneNotSupportedException {
        EnermyPlayer enermyPlayer=(EnermyPlayer) super.clone();
        /*
        若存在实现的类中还有类的情况
        则需要使用 enermyPlayer.具体的类.clone
         */
        return enermyPlayer;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    protected EnermyPlayer deepCloneBySerialize() throws CloneNotSupportedException, IOException, ClassNotFoundException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(byteArrayOutputStream);
        oos.writeObject(this);

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(byteArrayInputStream);
        return (EnermyPlayer) ois.readObject();


    }
}
