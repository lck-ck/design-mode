package UI;

import java.awt.image.BufferedImage;

public class Plane {

    BufferedImage image;
    int x;
    int y;

    private static class singletonHolder {
        private static final Plane plane = new Plane();
    }


    private Plane() {
        image = GetImg.getImg("hero.png");
        x = 300;
        y = 500;
    }
    public static Plane getplane() {
        return singletonHolder.plane;
    }

}