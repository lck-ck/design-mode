package UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Panel extends JPanel {

    BufferedImage bg = GetImg.getImg("bg1.jpg");
    List<EnermyPlayer> enermyPlayers = new ArrayList<>();
    List<Fire> fires = new ArrayList<>();
    Plane plane = Plane.getplane();
    Plane plane1 = Plane.getplane();
    EnermyPlayer enermyPlayer = new EnermyPlayer();

    public void begin() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        epCreate();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                    epMove();
                    hit();
                    try {
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }

        }.start();
    }

    private void epMove() {
        for (int i = 0; i < enermyPlayers.size(); i++) {
            EnermyPlayer enermyPlayer = enermyPlayers.get(i);
            enermyPlayer.y = enermyPlayer.y + 10;
        }
    }

    int index = 0;

    private void epCreate() throws CloneNotSupportedException {
        index++;
        if (index >= 20) {
            EnermyPlayer enermyPlayer1= (EnermyPlayer) enermyPlayer.clone();
            Random random=new Random();
            int index1=random.nextInt(15)+1;
            String path = (index1<10)?("0"+index1):(""+index1);
            String name="ep"+path+".png";
            enermyPlayer1.image=GetImg.getImg(name);
            enermyPlayer1.y=0;
            enermyPlayer1.x=random.nextInt(512- enermyPlayer1.image.getWidth());
            enermyPlayers.add(enermyPlayer1);
            index = 0;

        }
    }

    //子弹的线程
    public void firebegin() {
        new Thread() {
            public void run() {
                while (true) {
                    fireCreate();
                    fireMove();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    repaint();
                }
            }
        }.start();
    }

    int score = 0;

    private void hit() {
        for (int i = 0; i < enermyPlayers.size(); i++) {
            EnermyPlayer enermyPlayer = enermyPlayers.get(i);
            for (int j = 0; j < fires.size(); j++) {
                Fire fire = fires.get(j);
                if (fire.x + fire.image.getWidth() / 4 > enermyPlayer.x && fire.x < enermyPlayer.x + enermyPlayer.image.getWidth() && fire.y + fire.image.getHeight() / 4 > enermyPlayer.y && fire.y < enermyPlayer.y + enermyPlayer.image.getHeight()) {
                    fires.remove(j);
                    enermyPlayers.remove(i);
                    score++;
                }
            }
        }
    }

    private void fireMove() {
        for (int i = 0; i < fires.size(); i++) {
            Fire fire = fires.get(i);
            fire.y = fire.y - 5;
        }
    }

    int index1 = 0;

    private void fireCreate() {

        index1++;
        if (index1 >= 20) {
            Fire fire = new Fire(plane);
            Fire fire1 = new Fire(plane1);
            fires.add(fire);
            fires.add(fire1);
            index1 = 0;

        }

    }

    public Panel(Frame frame) {
        MouseAdapter mouseAdapter = new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                plane1.x = e.getX() - plane1.image.getWidth() / 2;
                plane1.y = e.getY() - plane1.image.getHeight() / 2;
                repaint();
            }

        };
        addMouseListener(mouseAdapter);
        addMouseMotionListener(mouseAdapter);

        KeyAdapter keyAdapter = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                int keyCode = e.getKeyCode();

                if (keyCode == KeyEvent.VK_KP_LEFT || keyCode == KeyEvent.VK_A) {
                    plane.x = plane.x - 10;
                    if (plane.x <= 0) {
                        plane.x = 0;
                    }
                } else if (keyCode == KeyEvent.VK_RIGHT || keyCode == KeyEvent.VK_D) {
                    plane.x = plane.x + 10;
                } else if (keyCode == KeyEvent.VK_DOWN || keyCode == KeyEvent.VK_S) {
                    plane.y = plane.y + 10;
                } else if (keyCode == KeyEvent.VK_UP || keyCode == KeyEvent.VK_W) {
                    plane.y = plane.y - 10;
                }
                repaint();
            }
        };
        frame.addKeyListener(keyAdapter);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(bg, 0, 0, null);
        g.drawImage(plane.image, plane.x, plane.y, null);
        g.drawImage(plane1.image, plane1.x, plane1.y, null);
        for (int i = 0; i < enermyPlayers.size(); i++) {
            EnermyPlayer enermyPlayer = enermyPlayers.get(i);
            g.drawImage(enermyPlayer.image, enermyPlayer.x, enermyPlayer.y, null);

        }
        for (int i = 0; i < fires.size(); i++) {
            Fire fire = fires.get(i);
            g.drawImage(fire.image, fire.x, fire.y, fire.image.getWidth() / 4, fire.image.getHeight() / 4, null);
        }
        g.setColor(Color.white);

        g.drawString("分数" + score, 10, 30);
    }

}
