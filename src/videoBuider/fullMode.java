package videoBuider;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 11:35
 */
public class fullMode extends builder {
    @Override
    public void displayMenuBuilder() {
        videoDisplay.setDisplayMenu("显示菜单功能");
    }

    @Override
    public void playListBuilder() {
        videoDisplay.setPlayList("显示播放列表功能");
    }

    @Override
    public void mainWindowsBuilder() {
        videoDisplay.setMainWindows("显示主菜单功能");
    }

    @Override
    public void controlBuilder() {
        videoDisplay.setControl("显示控制条功能");
    }

    @Override
    public void collectList() {
        videoDisplay.setCollectList("显示收藏列表功能");
    }
}
