package videoBuider;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 11:42
 */
/*
        builder.controlBuilder();
        builder.playListBuilder();
        builder.collectList();
        builder.displayMenuBuilder();
        builder.mainWindowsBuilder();
 */

public class Director {
    public VideoDisplay construct(builder builder){
        VideoDisplay videoDisplay;
        if (builder.isSimpleMode())
        {
            builder.displayMenuBuilder();
            builder.controlBuilder();
        }
        else if (builder.isMemoryMode())
        {
            builder.displayMenuBuilder();
            builder.controlBuilder();
            builder.collectList();
        }
        else {
            builder.controlBuilder();
            builder.playListBuilder();
            builder.collectList();
            builder.displayMenuBuilder();
            builder.mainWindowsBuilder();
        }

        return builder.CreateVideoDispay();

    }
}
