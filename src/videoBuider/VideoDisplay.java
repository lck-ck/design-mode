package videoBuider;


/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 11:25
 */
public class VideoDisplay {
    private String displayMenu;
    private String playList;
    private String mainWindows;
    private String control;
    private String collectList;

    @Override
    public String toString() {
        return "VideoDisplay{" +
                "displayMenu='" + displayMenu + '\'' +
                ", playList='" + playList + '\'' +
                ", mainWindows='" + mainWindows + '\'' +
                ", control='" + control + '\'' +
                ", collectList='" + collectList + '\'' +
                '}';
    }

    public String getDisplayMenu() {
        return displayMenu;
    }

    public void setDisplayMenu(String displayMenu) {
        this.displayMenu = displayMenu;
    }

    public String getPlayList() {
        return playList;
    }

    public void setPlayList(String playList) {
        this.playList = playList;
    }

    public String getMainWindows() {
        return mainWindows;
    }

    public void setMainWindows(String mainWindows) {
        this.mainWindows = mainWindows;
    }

    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    public String getCollectList() {
        return collectList;
    }

    public void setCollectList(String collectList) {
        this.collectList = collectList;
    }

    public VideoDisplay(String displayMenu, String playList, String mainWindows, String control, String collectList) {
        this.displayMenu = displayMenu;
        this.playList = playList;
        this.mainWindows = mainWindows;
        this.control = control;
        this.collectList = collectList;
    }

    public VideoDisplay() {
    }
}
