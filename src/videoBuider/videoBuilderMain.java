package videoBuider;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 11:24
 */
public class videoBuilderMain {
    public static void main(String[] args) {
        VideoDisplay videoDisplay;
        builder builder = (videoBuider.builder) XMLReadVideo.getBrand();
        Director director = new Director();
        videoDisplay = director.construct(builder);
        System.out.println(videoDisplay);
    }
}
