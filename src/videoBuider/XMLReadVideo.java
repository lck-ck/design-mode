package videoBuider;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.lang.reflect.Constructor;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 11:58
 */
public class XMLReadVideo {
    public static Object getBrand() {

        try {

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            Document document = documentBuilder.parse(new File("config.xml"));


            NodeList nodeList = document.getElementsByTagName("brandName");

            Node node = nodeList.item(0).getFirstChild();

            String name = node.getNodeValue().trim();


            Class c = Class.forName("videoBuider."+name);


            Constructor constructor = c.getDeclaredConstructor();

            constructor.setAccessible(true);

            Object o = constructor.newInstance();

            return o;


        } catch (Exception e) {

            throw new RuntimeException("UnSupportedShapeException");



        }

    }

}

