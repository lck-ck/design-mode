package videoBuider;

import buider.AngleBuilder;

import javax.lang.model.element.VariableElement;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 11:31
 */
public abstract class builder {

    VideoDisplay videoDisplay=new VideoDisplay();
    public abstract void displayMenuBuilder();
    public abstract void playListBuilder();
    public abstract void mainWindowsBuilder();
    public abstract void controlBuilder();
    public abstract void collectList();
    public Boolean isSimpleMode()
    {
        return false;
    }
    public Boolean isMemoryMode()
    {
        return false;
    }
    public Boolean isInternetMode()
    {
        return false;
    }
    public VideoDisplay CreateVideoDispay()
    {
        return videoDisplay;
    }
}
