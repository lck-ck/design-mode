package strategy;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 10:34
 */
public class BubbleSort implements Sort{
    @Override
    public int[] sort(int[] array) {
        int len=array.length;
        int temp;
        for (int i = 0; i < len - 1; i++) {
            for (int j = 0; j < len-1-i; j++) {
                if (array[j]>array[j+1])
                {
                    temp=array[j];
                    array[j]=array[j+1];
                    array[j+1]=temp;
                }
            }
        }
        System.out.println("冒泡排序完成");
        return array;
    }
}
