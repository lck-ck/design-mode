package strategy;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 10:38
 */
public class SelectionSort implements Sort {
    @Override
    public int[] sort(int[] array) {
        int len = array.length;
        int temp;
        for (int i = 0; i < len - 1; i++) {
            for (int j = i + 1; j < len; j++) {
                if (array[i] > array[j]) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println("选择排序完成");
        return array;
    }
}
