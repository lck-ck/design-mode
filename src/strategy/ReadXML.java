package strategy;

import com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.lang.reflect.Constructor;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 10:49
 */
public class ReadXML {
    public static Object getBean() {
        try {
            DocumentBuilderFactory documentBuilderFactory = new DocumentBuilderFactoryImpl();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(new File("src/strategy/config.xml"));
            NodeList nodeList = document.getElementsByTagName("className");
            Node node = nodeList.item(0).getFirstChild();
            String name = node.getNodeValue().trim();
            Class c = Class.forName("strategy."+name);
            Constructor constructor = c.getDeclaredConstructor();
//            拿私有构造器
//          constructor.setAccessible(true);
            constructor.newInstance();
            Object o=constructor.newInstance();
            return o;

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }
}
