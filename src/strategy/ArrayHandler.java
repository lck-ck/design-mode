package strategy;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 10:41
 */
public class ArrayHandler {
    private Sort sortObj;

    public void setSortObj(Sort sortObj) {
        this.sortObj = sortObj;
    }

    public int[] sort(int [] array)
    {
        return sortObj.sort(array);
    }

}
