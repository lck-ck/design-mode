package strategy;

import factory.XMLRead;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 10:42
 */
public class ClientMain {
    public static void main(String[] args) {
        int[]array={5,3,2,1,10,8,9,7,6,4};
        int [] result;
        ArrayHandler arrayHandler=new ArrayHandler();
        Sort sort= (Sort) ReadXML.getBean();
        arrayHandler.setSortObj(sort);
        result=arrayHandler.sort(array);
        for (int i : result) {
            System.out.print(i+" ");
        }
    }
}
