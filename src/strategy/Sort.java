package strategy;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 10:33
 */
public interface Sort {
    int[] sort(int[] array);
}
