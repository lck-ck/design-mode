package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 15:47
 */
public class HaierFactory implements Factory {
    @Override
    public TV produceTV() {
        return new HaierTV();
    }

    @Override
    public Phone producePhone() {
        return new HaierPhone();
    }
}
