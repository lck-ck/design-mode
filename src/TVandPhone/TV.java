package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 15:28
 */
public interface TV {
    public void play();
}
