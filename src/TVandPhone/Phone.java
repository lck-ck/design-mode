package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 16:09
 */
public interface Phone {
    void play();
}
