package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 16:12
 */
public class HuaiWeiPhone implements Phone{
    @Override
    public void play() {
        System.out.println("华为手机正在播放");
    }
}
