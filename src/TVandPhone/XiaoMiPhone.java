package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 16:14
 */
public class XiaoMiPhone implements Phone{
    @Override
    public void play() {
        System.out.println("小米手机正在播放中");
    }
}
