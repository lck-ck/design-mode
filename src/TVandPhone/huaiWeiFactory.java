package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 16:03
 */
public class huaiWeiFactory implements Factory{
    @Override
    public TV produceTV() {
        return new HuaiWeiTV();
    }

    @Override
    public Phone producePhone() {
        return new HuaiWeiPhone();
    }
}
