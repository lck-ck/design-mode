package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 15:23
 */
public class main2 {
    public static void main(String[] args) {
        TV tv;
        Phone phone;
        Factory factory;
        factory = (Factory) XMLReadTVandPhone.getBrand();
        tv= factory.produceTV();
        tv.play();
        phone = factory.producePhone();
        phone.play();

    }
}
