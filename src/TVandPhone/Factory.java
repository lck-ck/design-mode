package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 15:19
 */
public interface Factory {
    TV produceTV();
    Phone producePhone();
}
