package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 16:11
 */
public class HisensePhone implements Phone{
    @Override
    public void play() {
        System.out.println("海信手机正在通话中");
    }
}
