package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 16:05
 */
public class XiaoMiTvFactory implements Factory{
    @Override
    public TV produceTV() {
        return new XiaoMiTV();
    }

    @Override
    public Phone producePhone() {
        return new XiaoMiPhone();
    }
}
