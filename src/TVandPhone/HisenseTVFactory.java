package TVandPhone;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/28 15:47
 */
public class HisenseTVFactory implements Factory {
    @Override
    public TV produceTV() {
        return new HisenseTV();
    }

    @Override
    public Phone producePhone() {
        return new HisensePhone();
    }
}
