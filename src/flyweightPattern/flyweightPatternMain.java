package flyweightPattern;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/19 12:34
 */
public class flyweightPatternMain {
    public static void main(String[] args) {
        NetworkDevice networkDevice1,networkDevice2,networkDevice3,networkDevice4;
        DeviceFactory deviceFactory=DeviceFactory.getInstance();
        networkDevice1=deviceFactory.getNetworkDevice("h");
        networkDevice2=deviceFactory.getNetworkDevice("h");
        networkDevice3=deviceFactory.getNetworkDevice("s");
        networkDevice4=deviceFactory.getNetworkDevice("s");
        networkDevice1.use(new Port("100"));
        networkDevice2.use(new Port("200"));
        networkDevice3.use(new Port("300"));
        networkDevice4.use(new Port("400"));
        System.out.println("networkDevice1.equals(networkDevice2) = " + networkDevice1.equals(networkDevice2));
        System.out.println("networkDevice3.equals(networkDevice4) = " + networkDevice3.equals(networkDevice4));


    }
}
