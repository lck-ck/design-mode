package flyweightPattern;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/19 12:24
 */
public class Port {
    private String port;

    public Port(String port) {
        this.port = port;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }


}
