package flyweightPattern;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/19 12:09
 */
public class Switch extends NetworkDevice {
    private String type;

    public Switch() {

    }

    public String getType() {
        return "交换机";
    }

    public Switch(String type) {
        this.type = type;
    }

}
