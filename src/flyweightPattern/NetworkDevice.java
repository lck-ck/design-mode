package flyweightPattern;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/19 11:45
 */
public abstract class NetworkDevice {
    public abstract String getType();
    public void use(Port port)
    {
        System.out.println(this.getType()+"使用端口为："+port.getPort());
    }

}
