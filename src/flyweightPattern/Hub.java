package flyweightPattern;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/19 12:12
 */
public class Hub extends NetworkDevice{
    private String type;

    public String getType() {
        return "集线器";
    }

    public Hub() {

    }

}
