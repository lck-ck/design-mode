package flyweightPattern;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/19 11:42
 */
public class DeviceFactory {
    private static DeviceFactory instance=new DeviceFactory();
    private HashMap devices=new HashMap();
    private int totalTerminal=0;

    public DeviceFactory() {
        NetworkDevice networkDevice1,networkDevice2;
        networkDevice1=new Switch();
        networkDevice2=new Hub();
        devices.put("s",networkDevice1);
        devices.put("h",networkDevice2);

    }
    public static DeviceFactory getInstance()
    {
        return instance;
    }
    public NetworkDevice getNetworkDevice(String type)
    {
        return (NetworkDevice) devices.get(type);
    }
    public int getTotalDevice()
    {
        return 0;
    }
    public int getTotalTerminal()
    {
        return 0;
    }
}
