package Union;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 15:29
 */
public class UnionMain {
    public static void main(String[] args) {
        UN chatWFC=new WFC();
        UN chatWFC1=new WHO();
        UN chatWFC2=new WTO();

        Country country1,country2;
        country1=new DevelopedCountry("美国");
        country2=new DevelopingCountry("中国");
        chatWFC.register(country1);
        chatWFC.register(country2);


        country2.sendNegotiate(country1.name,"你好呀");
    }
}
