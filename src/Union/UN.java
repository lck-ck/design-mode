package Union;

import org.w3c.dom.views.AbstractView;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 15:07
 */
public abstract class UN {
    public abstract void register(Country country);
    public abstract void sendNegotiate(String from, String to, String message);
}
