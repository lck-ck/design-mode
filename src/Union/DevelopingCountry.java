package Union;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 15:21
 */
public class DevelopingCountry extends Country{
    public DevelopingCountry(String name) {
        super(name);
    }

    @Override
    public void sendNegotiate(String to, String message) {
        System.out.print("发展中国家");
        this.un.sendNegotiate(this.name,to,message);
    }

    @Override
    public void receiveNegotiate(String from, String message) {
        System.out.println(from+"消息是"+message);
    }
}
