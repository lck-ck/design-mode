package Union;

import java.util.Hashtable;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 15:19
 */
public class WFC extends UN{
    private Hashtable member=new Hashtable();
    @Override
    public void register(Country country) {
        if (!member.contains(country))
        {
            member.put(country.name,country);
        }
        country.setUn(this);

    }

    @Override
    public void sendNegotiate(String from, String to, String message) {
        System.out.println(from+"发给"+to+"消息："+message);
        System.out.println("这是WTO聊天室");
    }
}
