package Union;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 15:09
 */
public abstract class Country {
    protected UN un;
    protected String name;

    public Country(String name) {
        this.name = name;
    }

    public UN getUn() {
        return un;
    }

    public void setUn(UN un) {
        this.un = un;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public abstract void sendNegotiate(String to,String message);
    public abstract void receiveNegotiate(String from,String message);
}
