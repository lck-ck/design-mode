package chainOfResponsibility;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/26 10:55
 */
public class GeneralManager extends Leader{
    public GeneralManager(String name) {
        super(name);
    }

    @Override
    public void handleRequest(LeaveRequest leaveRequest) {
        if (leaveRequest.getMoney()<20)
        {
            System.out.println(this.name+"总经理审批通过"+leaveRequest.getName()+"，审批单价格为"+leaveRequest.getMoney()+"万元");
        }
        else {
            System.out.println("采购单需要开职工大会确定");
        }
    }
}
