package chainOfResponsibility;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/26 10:50
 */
public class Director extends Leader{
    public Director(String name) {
        super(name);
    }

    @Override
    public void handleRequest(LeaveRequest leaveRequest) {
        if (leaveRequest.getMoney()<1)
        {
            System.out.println(this.name+"主任审批通过"+leaveRequest.getName()+"，审批单价格为"+leaveRequest.getMoney()+"万元");
        }
        else {
            next.handleRequest(leaveRequest);
        }
    }
}
