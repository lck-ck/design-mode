package chainOfResponsibility;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/26 11:00
 */
public class ViceGeneralManager extends Leader{
    public ViceGeneralManager(String name) {
        super(name);
    }

    @Override
    public void handleRequest(LeaveRequest leaveRequest) {
        if (leaveRequest.getMoney()<10)
        {
            System.out.println(this.name+"副总经理审批通过"+leaveRequest.getName()+"，审批单价格为"+leaveRequest.getMoney()+"万元");
        }
        else {
            next.handleRequest(leaveRequest);
        }
    }
}
