package chainOfResponsibility;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/26 10:40
 */
public class chainMain {
    public static void main(String[] args) {
        LeaveRequest leaveRequest=new LeaveRequest("卢常锴",20);
        Leader director,generalManager,viceGeneralManager,department;

        director=new Director("张三");
        department=new departmentManager("孙七");
        generalManager=new GeneralManager("王五");
        viceGeneralManager=new ViceGeneralManager("赵六");

        director.setNext(department);
        department.setNext(viceGeneralManager);
        viceGeneralManager.setNext(generalManager);

        director.handleRequest(leaveRequest);
    }
}
