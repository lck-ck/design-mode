package chainOfResponsibility;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/26 10:35
 */
public class LeaveRequest {
    public LeaveRequest(String name, int money) {
        this.name = name;
        this.money = money;
    }

    private  String name;
    private int money;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "LeaveRequest{" +
                "name='" + name + '\'' +
                ", money=" + money +
                '}';
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }
}
