package chainOfResponsibility;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/26 10:44
 */
public abstract class Leader {
    protected String name;
    protected Leader next;
    public abstract void handleRequest(LeaveRequest leaveRequest);
    public Leader(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Leader getNext() {
        return next;
    }

    public void setNext(Leader next) {
        this.next = next;
    }

    public Leader(String name, Leader next) {
        this.name = name;
        this.next = next;
    }
}
