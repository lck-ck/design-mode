package Account;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 15:56
 */
public class NormalState extends AccountState{
    public NormalState(Account acc) {
        this.state=acc;
    }

    public NormalState(AccountState state) {
        this.state=state.state;

    }

    @Override
    public void deposit(double amount) {
        this.state.setBalance(this.state.getBalance()+amount);
        stateCheck();
    }

    @Override
    public void withdraw(double amount) {
        this.state.setBalance(this.state.getBalance()-amount);
        stateCheck();
    }

    @Override
    public void computerInterest() {

    }
}
