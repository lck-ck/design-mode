package Account;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 16:05
 */
public class AccountMain {
    public static void main(String[] args) {
        Account account=new Account("张山",20000);
       account.withdraw(100);
       account.withdraw(2000000);
       account.deposit(200000);

    }
}
