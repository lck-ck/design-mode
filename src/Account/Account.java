package Account;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 15:16
 */
public class Account {
    private AccountState state;
    private String owner;
    private double balance;

    public Account(String name,double balance) {
        this.owner=name;
        this.balance=balance;
        this.state=new NormalState(this);
        System.out.println(this.owner+"开户的初始金额"+this.balance);

    }

    public AccountState getState() {
        return state;
    }

    public void setState(AccountState state) {
        this.state = state;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    public void deposit(double amount){
        System.out.println(owner+"存款");
        state.deposit(amount);
        System.out.println("现在账户状态为"+state.getClass().getName());
    }
    public void withdraw(double amount){
        System.out.println("取钱");
        state.withdraw(amount);
        System.out.println("现在账户状态为"+state.getClass().getName());

    }
    public void computeInterest(){
        state.computerInterest();
    }


}
