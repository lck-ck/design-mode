package Account;

import Forum.AbstractState;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/10 15:18
 */
public abstract class AccountState {
    protected Account state;


    public abstract void deposit(double amount);
    public abstract void withdraw(double amount);
    public abstract void computerInterest();

    public Account getState() {
        return state;
    }

    public void setState(Account state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "AccountState{" +
                "state=" + state.getState() +
                '}';
    }

    public  void stateCheck()
    {
        if (this.state.getBalance()>0)
        {
            this.state.setState(new NormalState(this));
        }else if (this.state.getBalance()<0&&this.state.getBalance()>=2000)
        {
            this.state.setState(new OverdraftState(this));
        }else
        {
            this.state.setState(new RestrictedState(this));
        }
    }


}
