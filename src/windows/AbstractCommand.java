package windows;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 15:20
 */
public abstract class AbstractCommand {
    public abstract void execute();
}
