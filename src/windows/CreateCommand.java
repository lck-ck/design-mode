package windows;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 15:27
 */
public class CreateCommand extends AbstractCommand{
    private MenuItem menu;

    public CreateCommand() {
        menu=new MenuItem();
    }

    @Override
    public void execute() {
    menu.CreateCommand();
    }
}
