package windows;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 15:28
 */
public class EditCommand extends AbstractCommand{
    private MenuItem menu;

    public EditCommand() {
        menu=new MenuItem();
    }

    @Override
    public void execute() {
        menu.EditCommand();
    }
}
