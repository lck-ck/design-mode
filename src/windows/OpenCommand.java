package windows;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 15:23
 */
public class OpenCommand extends AbstractCommand {
    private MenuItem menu;

    public OpenCommand() {
        menu=new MenuItem();
    }

    @Override
    public void execute() {
        menu.OpenCommand();
    }
}
