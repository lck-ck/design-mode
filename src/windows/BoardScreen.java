package windows;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 15:22
 */
public class BoardScreen {
    private AbstractCommand command1,command2,command3;

    public BoardScreen() {
        command1=new CreateCommand();
        command2=new OpenCommand();
        command3=new EditCommand();
    }
    public void open()
    {
        command1.execute();
    }
    public void create()
    {
        command2.execute();
    }
    public void edit()
    {
        command3.execute();
    }
}
