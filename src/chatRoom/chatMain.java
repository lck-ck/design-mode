package chatRoom;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 14:52
 */
public class chatMain {
    public static void main(String[] args) {
        AbstractChatRoom chatRoom=new Chatroom();
        Member member1,member2,member3,member4,member5;
        member1=new CommonMember("张三");
        member2=new CommonMember("李四");
        member3=new Diamond("王五");
        member4=new Diamond("赵六");
        member5=new Diamond("唐七");

        chatRoom.register(member1);
        chatRoom.register(member2);
        chatRoom.register(member3);
        chatRoom.register(member4);
        chatRoom.register(member5);

        member1.sendText(member2.name,"今天天气很好，出去玩吗，操" );
        member5.sendImage(member2.name,"蒙拉里莎。jpg");
    }
}
