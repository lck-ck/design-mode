package chatRoom;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 14:33
 */
public abstract class Member {
    protected AbstractChatRoom chatroom;
    protected String name;

    public Member(String name) {
        this.name = name;
    }

    public AbstractChatRoom getChatroom() {
        return chatroom;
    }

    public void setChatroom(AbstractChatRoom chatroom) {
        this.chatroom = chatroom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void receiveText(String from, String message) {
        System.out.println(this.name + "收到" + from + "发来的信息" + message);

    }

    public void receiveImage(String from, String image) {
        System.out.println(this.name + "收到" + from + "发来的图片" + image);
    }
    public abstract void sendText(String to,String message);
    public abstract void sendImage(String to,String image);
}
