package chatRoom;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 14:44
 */
public class Diamond extends Member{
    public Diamond(String name) {
        super(name);
    }

    @Override
    public void sendText(String to, String message) {
        System.out.print("钻石会员");
        this.chatroom.sendText(this.name,to,message);
    }

    @Override
    public void sendImage(String to, String image) {
        System.out.print("钻石会员");
        this.chatroom.sendText(this.name,to,image);
    }
}
