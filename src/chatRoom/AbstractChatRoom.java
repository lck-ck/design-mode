package chatRoom;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 14:34
 */
public abstract class AbstractChatRoom {
    public abstract void register(Member member);
    public abstract void sendText(String from,String to,String message);
    public abstract void sendImage(String from,String to,String image);

}
