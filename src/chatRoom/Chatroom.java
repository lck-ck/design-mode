package chatRoom;

import java.util.Hashtable;
import java.util.List;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 14:45
 */
public class Chatroom extends AbstractChatRoom{
    private Hashtable members=new Hashtable();
    @Override
    public void register(Member member) {
        if (!members.contains(member))
        {
            members.put(member.name,member);
        }
        member.setChatroom(this);
    }

    @Override
    public void sendText(String from, String to, String message) {
        String newMessage=message.replace("日","*");
        newMessage=message.replace("操","*");
        System.out.println(from+"发给"+to+"消息："+newMessage);

    }

    @Override
    public void sendImage(String from, String to, String image) {
        System.out.println(from+"发给"+to+"消息："+image);
    }
}
