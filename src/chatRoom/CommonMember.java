package chatRoom;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/3 14:41
 */
public class CommonMember extends Member{
    public CommonMember(String name) {
        super(name);
    }

    @Override
    public void sendText(String to, String message) {
        System.out.print("普通会员");
        this.chatroom.sendText(this.name,to,message);
    }

    @Override
    public void sendImage(String to, String image) {
        System.out.println("普通会员不能发图");
    }
}
