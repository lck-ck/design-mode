package MovieTicket;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 11:13
 */
public class StudentDiscount implements Discount {

    @Override
    public double calculate(double price) {
        System.out.println("学生票");
        return price*0.8;
    }
}
