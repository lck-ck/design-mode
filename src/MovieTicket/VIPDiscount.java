package MovieTicket;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 11:15
 */
public class VIPDiscount implements Discount{
    @Override
    public double calculate(double price) {
        System.out.println("VIP会员");
        return price=price/2;
    }
}
