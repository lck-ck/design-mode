package MovieTicket;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 11:11
 */
public interface Discount {
    double calculate(double price);
}
