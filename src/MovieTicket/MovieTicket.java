package MovieTicket;

import jdk.nashorn.internal.codegen.DumpBytecode;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 11:11
 */
public class MovieTicket {
    private double price;
    private Discount discount;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }
}
