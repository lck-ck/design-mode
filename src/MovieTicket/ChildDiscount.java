package MovieTicket;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 11:14
 */
public class ChildDiscount implements Discount{
    @Override
    public double calculate(double price) {
        System.out.println("儿童票优惠10元");
        return price-10;
    }
}
