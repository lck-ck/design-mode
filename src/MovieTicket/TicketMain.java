package MovieTicket;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/16 11:18
 */
public class TicketMain {
    public static void main(String[] args) {
        MovieTicket ticket = new MovieTicket();
        Discount discount = (Discount) ReadXML.getBean();
        double result = discount.calculate(200);

        System.out.println(result);
    }
}
