package adapter;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/29 14:28
 */
public class Dog {
    public void wang(){
        System.out.println("狗在汪汪叫");
    }
    public void run()
    {
        System.out.println("狗在地上跑");
    }

}
