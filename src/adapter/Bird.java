package adapter;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/29 14:37
 */
public class Bird {
    public void tweedle()
    {
        System.out.println("鸟在叽叽喳喳叫");
    }
    public void fly()
    {
        System.out.println("鸟在飞翔");
    }
}
