package adapter;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/29 14:30
 */
public interface Robot {
    void cry();
    void move();
}
