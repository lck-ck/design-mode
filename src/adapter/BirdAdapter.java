package adapter;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/29 14:37
 */
public class BirdAdapter implements Robot{
    private Bird bird;

    public BirdAdapter() {
        bird=new Bird();
    }

    @Override
    public void cry() {
        System.out.println("机器人模仿");
        bird.tweedle();
    }

    @Override
    public void move() {
        System.out.println("机器人模仿");
        bird.fly();
    }
}
