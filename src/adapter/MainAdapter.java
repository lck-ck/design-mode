package adapter;

import java.awt.*;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/29 14:34
 */
public class MainAdapter {
    public static void main(String[] args) {

        Robot robot=new BirdAdapter();
        robot.cry();
        robot.move();
    }
}
