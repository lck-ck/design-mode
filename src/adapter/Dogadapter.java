package adapter;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/29 14:33
 */
public class Dogadapter implements Robot {
    private Dog dog;

    public Dogadapter() {
        dog=new Dog();
    }
    @Override
    public void cry() {
        System.out.println("机器人模仿");
        dog.wang();
    }

    @Override
    public void move() {
        System.out.println("机器人模仿！");
        dog.run();
    }
}
