package adapter;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/9/29 14:54
 */
public class AdapterBoth implements Robot {
    private Bird bird;
    private Dog dog;
    public AdapterBoth() {
        bird=new Bird();
        dog=new Dog();
    }

    @Override
    public void cry() {
        System.out.println("机器人模仿");
        bird.tweedle();
        dog.wang();
    }

    @Override
    public void move() {
        System.out.println("机器人模仿");
        bird.fly();
        dog.run();
    }
}
