package KFC;

import com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.lang.reflect.Constructor;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/5 10:46
 */
public class ReadXml {
    public static Object getBean(String classname) {
        try {
            DocumentBuilderFactory builderFactory = new DocumentBuilderFactoryImpl();
            DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(new File("src/KFC/config.xml"));
            NodeList nodeList = document.getElementsByTagName("className");
            Node node = null;
            if (classname.equalsIgnoreCase("cup")) {
                node = nodeList.item(0).getFirstChild();
            } else if (classname.equalsIgnoreCase("sort")) {
                node = nodeList.item(1).getFirstChild();
            }
            String name = node.getNodeValue().trim();
            Class c = Class.forName("KFC."+name);
            Constructor constructor = c.getDeclaredConstructor();
            Object o = constructor.newInstance();
            return o;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
