package KFC;

import KFC.ReadXml;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/5 11:33
 */
public class KFCMain {
    public static void main(String[] args) {
        Cup cup;
        Sort sort;
        cup = (Cup) ReadXml.getBean("cup");
        sort = (Sort) ReadXml.getBean("sort");
        sort.setCup(cup);
        sort.drink("饮料");
    }
}
