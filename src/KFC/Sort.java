package KFC;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/5 11:23
 */
public abstract class Sort {
    protected Cup cup;

    public void setCup(Cup cup) {
        this.cup = cup;
    }
    public abstract void drink(String name);
}
