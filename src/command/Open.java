package command;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:36
 */
public class Open extends AbstractCommand{
    private Television tv;

    public Open() {
        tv=new Television();
    }

    @Override
    public void execute() {
        tv.open();
    }
}
