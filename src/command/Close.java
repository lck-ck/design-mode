package command;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:37
 */
public class Close extends AbstractCommand{
    private Television tv;

    public Close() {
        tv=new Television();
    }

    @Override
    public void execute() {
        tv.close();
    }
}
