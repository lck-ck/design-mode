package command;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:33
 */
public class MainCommand {
    public static void main(String[] args) {
        Controller controller=new Controller();
        controller.open();
        controller.change();
        controller.close();
    }
}
