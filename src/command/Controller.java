package command;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:30
 */
public class Controller {
 private AbstractCommand command1,command2,command3;

    public Controller() {
        command1=new Open();
        command2=new Close();
        command3=new Change();
    }
    public void open()
    {
        command1.execute();
    }
    public void close()
    {
        command2.execute();
    }public void change()
    {
        command3.execute();
    }
}
