package command;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:29
 */
public class Television {
    public void open()
    {
        System.out.println("打开电视机");
    }
    public void change()
    {
        System.out.println("换台");
    }
    public void close()
    {
        System.out.println("关闭电视机");
    }
}
