package command;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:38
 */
public class Change extends AbstractCommand{
    private Television tv;

    public Change() {
        tv=new Television();
    }

    @Override
    public void execute() {
        tv.change();
    }
}
