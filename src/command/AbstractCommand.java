package command;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/10/27 14:35
 */
public abstract class AbstractCommand {
    public abstract void execute();
}
