package Weather;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 11:20
 */
public class WeatherData extends Subject{
    @Override
    public void notifyObserve() {
        System.out.println("气象台发布信息更新");
        for (Object o : observes) {
            ((Observe)o).update();
        }
    }
}
