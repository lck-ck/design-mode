package Weather;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 11:15
 */
public abstract class Observe {
    public abstract void update();
}
