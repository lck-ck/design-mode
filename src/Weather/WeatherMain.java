package Weather;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 11:24
 */
public class WeatherMain {
    public static void main(String[] args) {
        Subject subject=new WeatherData();
        Observe observe1,observe2,observe3;
        observe1=new Baidu();
        observe2=new CurrentCondition();
        observe3=new Sina();
        subject.registerObserve(observe1);
        subject.registerObserve(observe2);
        subject.registerObserve(observe3);
        subject.notifyObserve();
    }
}
