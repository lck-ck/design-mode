package Weather;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LuChangKai
 * @version 1.0
 * @date 2022/11/9 11:13
 */
public abstract class Subject {
    protected ArrayList observes = new ArrayList();

    public void registerObserve(Observe observe) {
        observes.add(observe);
    }

    public void remove(Observe observe){
        observes.remove(observe);
    }
    public abstract void notifyObserve();
}
